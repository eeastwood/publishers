rsxml_publisher = {
    "username": "zoro",
    "password": "thefox",
    "host": "qazmq01a",
    "port": 5672,
    "publish_exchange": "rsxml_inbound",
    "publish_exchange_type": "fanout",
    "routing_key": "1234"
}

rsjson_publisher = {
    "username": "zoro",
    "password": "thefox",
    "host": "qazmq01a",
    "port": 5672,
    "publish_exchange": "rsjson_inbound",
    "publish_exchange_type": "topic",
    "routing_key": "1234"
}

zjson_publisher = {
    "username": "zoro",
    "password": "thefox",
    "host": "qazmq01a",
    "port": 5672,
    "publish_exchange": "zjson_inbound",
    "publish_exchange_type": "topic",
    "routing_key": "1234"
}

logging = {
    "host": "qalgs01a",
    "port": 5958,
    "level": "DEBUG"
}
