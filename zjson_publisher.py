import sys
import json
import os
import traceback
import logging

import zoro_messaging_queue.zmq as zmq
import zoro_logging.logstash
import config

logger = logging.getLogger("outbound_etl_utilities")
logstash_handler = zoro_logging.logstash.get_handler(
                        host=config.logging["host"],
                        port=config.logging["port"])
logger.addHandler(logstash_handler)
logger.setLevel(config.logging["level"])

zmq_setting = {
    'userid': config.zjson_publisher["username"],
    'password': config.zjson_publisher["password"],
    'hostname': config.zjson_publisher["host"],
    'port': config.zjson_publisher["port"],
    'exchange_name': config.zjson_publisher["publish_exchange"],
    'exchange_type': config.zjson_publisher["publish_exchange_type"],
    'routing_key': config.zjson_publisher["routing_key"]
}

filename = sys.argv[1]

with open(filename) as f:
    try:
        f = json.load(f)
        payload = json.dumps(f)
        zmq.publish_message(payload, **zmq_setting)
        logger.debug("Successfully published zjson message")
    except Exception as e:
        log_obj = {
            "error": "Failed to publish zjson message",
            "exception": e,
            "traceback": traceback.format_exc()
        }
        logger.error(log_obj)
